libapp-info-perl (0.57-4) unstable; urgency=medium

  [ Debian Janitor ]
  * Apply multi-arch hints. + libapp-info-perl: Add Multi-Arch: foreign.
  * Bump debhelper from old 12 to 13.
  * Update standards version to 4.5.1, no changes needed.
  * Update standards version to 4.6.0, no changes needed.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Tue, 06 Dec 2022 22:41:38 +0000

libapp-info-perl (0.57-3) unstable; urgency=medium

  [ Salvatore Bonaccorso ]
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ Laurent Baillet ]
  * fix lintian file-contains-trailing-whitespace warning

  [ gregor herrmann ]
  * Drop build dependency on libsqlite0-dev.
    Thanks to Simon McVittie for the bug report.
    (Closes: #949661)
  * Annotate test-only build dependencies with <!nocheck>.
  * Add debian/upstream/metadata.
  * Declare compliance with Debian Policy 4.5.0.
  * Bump debhelper-compat to 12.
  * debian/watch: use uscan version 4.
  * Replace build dependency on libdbd-sqlite3-perl with sqlite3.
  * Update years of packaging copyright.

 -- gregor herrmann <gregoa@debian.org>  Thu, 23 Jan 2020 23:48:23 +0100

libapp-info-perl (0.57-2) unstable; urgency=medium

  * Team upload.

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend

  [ Niko Tyni ]
  * Make the package autopkgtestable
  * Update to Standards-Version 3.9.6
  * Add explicit build dependency on libmodule-build-perl
  * Extend build dependencies for better test suite coverage

 -- Niko Tyni <ntyni@debian.org>  Sat, 06 Jun 2015 21:48:25 +0300

libapp-info-perl (0.57-1) unstable; urgency=medium

  * Take over for the Debian Perl Group; Closes: #733549 -- RFA/ITA
  * debian/control: Added: Vcs-Git field (source stanza); Vcs-Browser
    field (source stanza). Changed: Homepage field changed to
    metacpan.org URL; Maintainer set to Debian Perl Group <pkg-perl-
    maintainers@lists.alioth.debian.org> (was: Debian QA Group
    <packages@qa.debian.org>).

  * debian/watch: use metacpan-based URL.
  * debian/rules: use dh(1).
  * debian/copyright: switch formatting to Copyright-Format 1.0.
  * Don't install README anymore.
  * Add /me to Uploaders.
  * Make short description a noun phrase.

  * New upstream release. Closes: #655035
  * Update years of upstream copyright.
  * Declare compliance with Debian Policy 3.9.5.

 -- gregor herrmann <gregoa@debian.org>  Sun, 12 Jan 2014 16:22:10 +0100

libapp-info-perl (0.56-2) unstable; urgency=low

  * Orphaned. Set maintainer to Debian QA Group.

 -- Nacho Barrientos Arias <nacho@debian.org>  Sun, 29 Dec 2013 21:43:10 +0000

libapp-info-perl (0.56-1) unstable; urgency=low

  * New upstream release.
  * Switch to dpkg-source 3.0 (quilt) format.
  * debian/control
   - Bump debhelper compatibility level to 8.
   - Set Standards-Version to 3.9.1 (no changes).
   - B-D: Remove libmodule-build-perl as it is provided by perl-modules.
   - B-D-I: Add libtest-pod-perl so zpod.t can be properly executed.
  * debian/rules
   - dh_clean -k -> dh_prep
   - Drop dh_strip (there are no executables or libraries to strip)

 -- Nacho Barrientos Arias <nacho@debian.org>  Tue, 15 Mar 2011 12:23:56 +0000

libapp-info-perl (0.55-1) unstable; urgency=low

  * New upstream release.
  * debian/control
   + Set Standards-Version to 3.8.0 (no changes).
   + Move homepage to the new Homepage field.
  * debian/rules
   + Prevent packlist creation.

 -- Nacho Barrientos Arias <nacho@debian.org>  Wed, 23 Jul 2008 11:07:56 +0200

libapp-info-perl (0.52-1) unstable; urgency=low

  * New upstream release.
  * debian/control
   - New maintainer email address.
  * debian/changelog
   - Updated copyright date.

 -- Nacho Barrientos Arias <nacho@debian.org>  Wed, 29 Aug 2007 12:59:36 +0200

libapp-info-perl (0.51-1) unstable; urgency=low

  * New upstream release.
  * debian/copyright
   - Adding accuracy to source download URL.

 -- Nacho Barrientos Arias <chipi@criptonita.com>  Wed, 27 Sep 2006 14:15:14 +0200

libapp-info-perl (0.50-1) unstable; urgency=low

  * New upstream release.
  * debian/copyright
  - Adding more information.
  - Fixing bad line wrapping (closes: #372597)
  - Adding packaging-related license terms.
  * debian/control
  - Bumping standards version to 3.7.2. (no changes needed)
  - Removing libtest as build-dep.
  - Fixing Homepage URL.
  * debian/rules
  - Using Makefile.
  - No longer running tests.
  - Removed commented lines.
  * debian/watch
  - Added.

 -- Nacho Barrientos Arias <chipi@criptonita.com>  Sun, 24 Sep 2006 13:44:26 +0200

libapp-info-perl (0.49-2) unstable; urgency=low

  * debian/control
  - Updating standards version to 3.7.2.
  - Changing Debhelper compat level to 5.
  * debian/copyright
  - Fixing problems with line wrapping. (closes: Bug#372597)

 -- Nacho Barrientos Arias <chipi@criptonita.com>  Mon, 12 Jun 2006 20:58:47 +0200

libapp-info-perl (0.49-1) unstable; urgency=low

  * Initial release (Closes: #362967)
  * Fixed HTTPD::Apache man file (=cut forgotten)

 -- Nacho Barrientos Arias <chipi@criptonita.com>  Sun, 16 Apr 2006 20:19:02 +0200
